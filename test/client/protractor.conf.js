exports.config = {
  baseUrl: 'http://localhost:3000',

  //seleniumAddress: 'http://localhost:4444/wd/hub',

  specs: ['e2e/*.js'],

  framework: 'mocha',
  mochaOpts: {
    reporter: 'spec',
    timeout: 4000
  }
};
