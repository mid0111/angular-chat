module.exports = function(config){
  config.set({

    basePath : '../../',

    files : [
      'public/app.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'test/client/unit/**/*.js'
    ],

    autoWatch : true,

    // For "Uncaught SyntaxError: Unexpected end of input"
    usePolling: false,

    frameworks: ['mocha', 'sinon-chai'],

    browsers : ['Chrome']

  });


};
