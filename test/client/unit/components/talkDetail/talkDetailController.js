'use strict';

describe('TalkDetailController', () => {

  beforeEach(module('app'));

  var $controller;
  var scope;
  var TalkService;

  beforeEach(inject((_$rootScope_, _$controller_, $q, _TalkService_) => {
    scope = _$rootScope_.$new();
    $controller = _$controller_;
    TalkService = _TalkService_;

    // spy response
    var defferd = $q.defer();
    defferd.resolve([{
      "account": {
        "img": "https://fqdn/profile_images/1234.jpeg",
        "name": "こじろー"
      },
      "text": "現在「とんかつ専門店」では、茶碗飯・味噌汁をセットにした和食のスタイルで「とんかつ」を提供している。",
      "createdAt": "2015/09/30 22:21"
    },{
      "account": {
        "img": "https://fqdn/profile_images/12345.jpeg",
        "name": "xxx"
      },
      "text" : "おはようございます。",
      "createdAt": "2015/10/01 13:22"
    }, {
      "account": {
        "img": "https://fqdn/profile_images/123456.jpeg",
        "name": "Tama"
      },
      "text" : "こんばんは。",
      "createdAt": "2015/10/05 8:00"
    }]);

    sinon.stub(TalkService, 'messages').returns({$promise: defferd.promise});
  }));

  beforeEach(() => {
    $controller('TalkDetailController as vm',{
      $scope: scope,
      TalkService: TalkService
    });
  });

  it('controller.messages にメッセージ一覧が設定されること', () => {
    expect(scope.vm.messages).to.be.an('undefined');

    scope.$apply();
    expect(scope.vm.messages).not.to.be.an('undefined');
    expect(scope.vm.messages).to.have.length(3);
  });

});
