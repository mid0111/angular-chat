'use strict';

describe('TalkListController', () => {

  beforeEach(module('app'));

  var $controller;
  var scope;
  var TalkService;

  beforeEach(inject(function(_$rootScope_, _$controller_, $q, _TalkService_) {
    scope = _$rootScope_.$new();
    $controller = _$controller_;
    TalkService = _TalkService_;

    // spy response
    var deferred = $q.defer();
    deferred.resolve([
      {
        "id": 1,
        "img": "https://fqdn/profile_images/1234.jpeg",
        "members": ["たま"]
      }, {
        "id": 2,
        "img": "https://fqdn2/profile_images/9876.jpeg",
        "members": ["ぽちたま", "じろー", "さすけん", "ぐりーん", "ぶるー"]
      }
    ]);
    sinon.stub(TalkService, 'query').returns({$promise: deferred.promise});
  }));

  beforeEach(() => {
    $controller('TalkListController as vm', {
      $scope: scope,
      TalkService: TalkService
    });
  });

  it('$scope.talks にトーク一覧が設定されること', () => {
    expect(scope.vm.talks).to.be.an('undefined');

    scope.$apply();
    expect(scope.vm.talks).not.to.be.an('undefined');
    expect(scope.vm.talks).to.have.length(2);

    expect(scope.vm.talks[0].id).to.equal(1);
    expect(scope.vm.talks[1].id).to.equal(2);
  });

});
