var chai = require('chai');
var expect = chai.expect;

describe('App', function() {

  it('should redirect / to /#/talks', function() {
    browser.get('/');
    browser.getLocationAbsUrl().then(function(url) {
      expect(url).to.equal('/talks');
    });
  });

});
