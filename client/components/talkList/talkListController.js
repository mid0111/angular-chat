'use strict';

export default class TalkListController {
  constructor(Session, TalkService) {
    TalkService.query().$promise.then((data) => {
      this.talks = data;
    });
  }
};

TalkListController.$inject = ['Session', 'TalkService'];
