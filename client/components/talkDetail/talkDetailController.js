'use strict';

export default class TalkDetailController {

  constructor(TalkService, $routeParams) {
    this.TalkService = TalkService;
    this.id = $routeParams.id;
    TalkService.messages({id: this.id}).$promise.then((data) => {
      this.messages = data;
    });
  }
};

TalkDetailController.$inject = ['TalkService', '$routeParams'];
