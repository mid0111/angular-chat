'use strict';

export default class LoginController {
  constructor($location, AuthService) {
    this.$location = $location;
    this.AuthService = AuthService;
  }

  login(credentials) {
    this.AuthService.login(credentials.email, credentials.password);
    this.$location.path('/');
  }
};

LoginController.$inject = ['$location', 'AuthService'];
