'use strict';

import angular from 'angular';
import {ngRoute} from 'angular-route';
import {ngResource} from 'angular-resource';

import Config from './shared/app.config.js';

import Session from './shared/sessionService.js';
import AuthService from './shared/authService.js';
import TalkService from './shared/talkService.js';

import LoginController from './components/login/loginController.js';
import TalkListController from './components/talkList/talkListController.js';
import TalkDetailController from './components/talkDetail/talkDetailController.js';


var app = angular.module('app',['ngRoute', 'ngResource'])
      .controller(LoginController.name, LoginController)
      .controller(TalkListController.name, TalkListController)
      .controller(TalkDetailController.name, TalkDetailController)

      .config(Config.routes)
      .service(Session.name, Session)

      .factory(AuthService.name, AuthService.createInstance)
      .factory(TalkService.name, TalkService.createInstance);

export {app};
