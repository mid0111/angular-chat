'use strict';

export default class TalkService {

  constructor($resource) {
    this.$resouce = $resource;
    return $resource('data/talks/:id', {}, {
      query: {method:'GET', params:{id:'talks'}, isArray:true},
      messages: {method: 'GET', url: 'data/talks/:id/messages', isArray: true}
    });
  }

  static createInstance($resource) {
    return TalkService.instance = new TalkService($resource);
  }
};

TalkService.createInstance.$inject = ['$resource'];
