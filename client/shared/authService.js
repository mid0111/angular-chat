'use strict';

export default class AuthService {

  constructor($resource, Session) {
    this.$resource = $resource;
    this.Session = Session;
  }

  static createInstance($resource, Session) {
    return AuthService.instance = new AuthService($resource, Session);
  }

  login(email, password) {
    var userId = '1234';
    var name = 'mid0111';
    this.Session.create(userId, name);
  }
};

AuthService.createInstance.$inject = ['$resource', 'Session'];
