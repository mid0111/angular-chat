'use strict';


export default class Session {
  create(id, name) {
    this.id = id;
    this.name = name;
  }

  destroy() {
    this.id = null;
    this.name = null;
  }
};
