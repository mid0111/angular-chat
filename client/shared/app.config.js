'use strict';

import LoginController from '../components/login/loginController.js';
import TalkListController from '../components/talkList/talkListController.js';
import TalkDetailController from '../components/talkDetail/talkDetailController.js';

export default class Config {

  static routes($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: '../components/login/login.html',
        controller: LoginController.name,
        controllerAs: 'vm'
      })
      .when('/talks', {
        templateUrl: '../components/talkList/talkList.html',
        controller: TalkListController.name,
        controllerAs: 'vm'
      })
      .when('/talks/:id', {
        templateUrl: '../components/talkDetail/talkDetail.html',
        controller: TalkDetailController.name,
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/talks'
      });
  }
};

Config.routes.$inject = ['$routeProvider'];
