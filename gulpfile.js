var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var watchify = require('gulp-watchify');
var babelify = require('babelify');
var path = require('path');

var conf = {
  src: 'client',
  dest: 'public'
};

function reload() {
  browserSync.reload({ stream: false });
};

var watching = false;
gulp.task('enable-watch-mode', () => {
  watching = true;
});
gulp.task('js', watchify((watchify) => {
  return gulp.src(path.join(conf.src, 'app.js'))
    .pipe(watchify({
      watch: watching,
      debug: watching,
      setup: function(b) {
        b.transform(babelify.configure({
          sourceMap: true,
          sourceMapRelative: __dirname
        }));
      }
    }))
    .pipe(gulp.dest(conf.dest));
}));
gulp.task('js-watch', reload);

gulp.task('css', () => {
  return gulp.src(path.join(conf.src, 'css/*.css'))
    .pipe(gulp.dest(path.join(conf.dest, 'css')));
});
gulp.task('css-watch', ['css'], reload);

gulp.task('html', () => {
  return gulp.src(path.join(conf.src, '**/*.html'))
    .pipe(gulp.dest(conf.dest));
});
gulp.task('html-watch', ['html'], reload);

// Unit test for client
var karmaServer = require('karma').Server;
gulp.task('unit', () => {
  new karmaServer.start({
    configFile: __dirname + '/test/client/karma.conf.js',
    singleRun: !watching
  });
});

// E2E test
var protractor = require("gulp-protractor").protractor;
var webdriver_update = protractor.webdriver_update;
var webdriver_standalone = protractor.webdriver_standalone;
gulp.task('webdriver_update', webdriver_update);
gulp.task('webdriver_standalone', ['webdriver_update'], webdriver_standalone);
gulp.task('e2e', ['webdriver_standalone', 'serve'], (cb) => {
  gulp.src(["./tests/client/e2e/**/*.js"])
    .pipe(protractor({
      configFile: "test/client/protractor.conf.js"
    }))
    .on('error', (e) => {
      throw e;
    })
    .on('end', () => {
      if(!watching) browserSync.exit();
      cb();
    });
});

gulp.task('serve', ['js', 'css', 'html'], function () {
  browserSync.init({
    server: {
      baseDir: conf.dest
    },
    open: false
  });

  if(watching) {
    gulp.watch(path.join(conf.dest, "app.js"), ['js-watch']);
    gulp.watch(path.join(conf.src, "css/*.css"), ['css-watch']);
    gulp.watch(path.join(conf.src, "**/*.html"), ['html-watch']);
  }
});

gulp.task('default', ['enable-watch-mode', 'serve', 'unit']);
gulp.task('test', ['unit', 'e2e']);
gulp.task('build', ['js', 'css', 'html']);
